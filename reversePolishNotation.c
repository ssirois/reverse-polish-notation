#include "reversePolishNotation.h"

void ReversePolishNotation(struct ReversePolishNotation* rpn, char* expression) {
  memset(rpn->expression, '\0', sizeof(rpn->expression));
  Stack(&(rpn->stack));

  strcpy(rpn->expression, expression);
}

int isOperator(char* token) {
  if (strcmp(token, "+") == 0 || strcmp(token, "-") == 0 || strcmp(token, "*") == 0 || strcmp(token, "/") == 0) {
      return 1;
  }

  return 0;
}

int isOperand(char* token) {
  char* e = NULL;
  (void) strtol(token, &e, 0);
  return e != NULL && *e == (char)0;
}

int add(int operand1, int operand2) {
  return operand1 + operand2;
}

int substract(int operand1, int operand2) {
  return operand1 - operand2;
}

int multiply(int operand1, int operand2) {
  return operand1 * operand2;
}

int divide(int operand1, int operand2) {
  return operand1 / operand2;
}

int doArithmetic(char* operator, char* operand1, char* operand2) {
  int result = 0;
  if (strcmp(operator, "+") == 0) {
    result = add(atoi(operand1), atoi(operand2));
  }
  else if (strcmp(operator, "-") == 0) {
    result = substract(atoi(operand1), atoi(operand2));
  }
  else if (strcmp(operator, "*") == 0) {
    result = multiply(atoi(operand1), atoi(operand2));
  }
  else if (strcmp(operator, "/") == 0) {
    result = divide(atoi(operand1), atoi(operand2));
  }

  printf("evaluating: %s %s %s = %d\n", operand1, operator, operand2, result);
  return result;
}

void evaluate(struct ReversePolishNotation* reversePolishNotation) {
  char* tokens;
  for (tokens = reversePolishNotation->expression; ; tokens = NULL) {
    char *token;
    token = strtok(tokens, " ");
    if (token == NULL)
      break;

    if (isOperator(token)) {
      char* operand2 = pop(&(reversePolishNotation->stack));
      char* operand1 = pop(&(reversePolishNotation->stack));
      char result[100];
      sprintf(result, "%d", doArithmetic(token, operand1, operand2));
      push(&(reversePolishNotation->stack), result);
    }
    else if (isOperand(token)) {
      push(&(reversePolishNotation->stack), token);
    }
  }

  char* result = pop(&(reversePolishNotation->stack));
  strcpy(reversePolishNotation->result, result);
}

