#include "stack.h"

void Stack(struct Stack* stack) {
  stack->top = -1;
  for (int i = 0; i < STACK_MAX_CAPACITY; ++i) {
    stack->array[i] = 0;
  }
}

int isEmpty(struct Stack* stack) {
  return stack->top < 0;
}

int isFull(struct Stack* stack) {
  return stack->top == STACK_MAX_CAPACITY;
}

void push(struct Stack* stack, char* newValue) {
  if (!isFull(stack)) {
    stack->array[++stack->top] = newValue;
  }
}

char* pop(struct Stack* stack) {
  if (!isEmpty(stack)) {
    return stack->array[stack->top--];
  }

  return STACK_NULL_VALUE;
}

