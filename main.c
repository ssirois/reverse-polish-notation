#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "reversePolishNotation.h"

int main() {
  char zeExpression[] = "3 2 2 + *";

  struct ReversePolishNotation reversePolishNotation;
  ReversePolishNotation(&reversePolishNotation, zeExpression);

  evaluate(&reversePolishNotation);
  printf("%s = %s\n", zeExpression, reversePolishNotation.result);

  return 0;
}

