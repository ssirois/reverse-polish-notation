#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "stack.h"

struct ReversePolishNotation {
  char expression[200];
  char result[100];
  struct Stack stack;
};
void ReversePolishNotation(struct ReversePolishNotation* rpn, char* expression);

int isOperator(char* token);
int isOperand(char* token);

int add(int operand1, int operand2);
int substract(int operand1, int operand2);

int multiply(int operand1, int operand2);
int divide(int operand1, int operand2);

void evaluate(struct ReversePolishNotation* rpn);
int doArithmetic(char* operator, char* operand1, char* operand2);

