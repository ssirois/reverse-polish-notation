#include <limits.h>
#include <stdio.h>

#define STACK_MAX_CAPACITY 10
#define STACK_NULL_VALUE NULL

struct Stack {
  int top;
  char* array[STACK_MAX_CAPACITY];
};
void Stack(struct Stack* stack);

int isEmpty(struct Stack* stack);
int isFull(struct Stack* stack);

void push(struct Stack* stack, char* newValue);
char* pop(struct Stack* stack);

